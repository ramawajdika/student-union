from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import *
from .models import *


# Create your tests here.

class LandingPageUnitTest(TestCase):
    def test_landingpage_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_landingpage_url_func_working(self):
        found = resolve(reverse('landing'))
        self.assertEqual(found.func,view_landing)
