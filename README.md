# Repository Tugas 1 PPW-D (kelompok 13) 

## Anggota kelompok
- Ihsan Muhammad Aulia: -
- Ramawajdi Kanishka Anwar : -
- Taliza Ayu Dwiputritaufik : -

## Status Website
[![pipeline status](https://gitlab.com/tp1-ppw-d-2018-kel-13/student-union/badges/master/pipeline.svg)](https://gitlab.com/tp1-ppw-d-2018-kel-13/student-union/commits/master)
[![coverage report](https://gitlab.com/tp1-ppw-d-2018-kel-13/student-union/badges/master/coverage.svg)](https://gitlab.com/tp1-ppw-d-2018-kel-13/student-union/commits/master)


## Link Website Herokuapp
[https://student-union-project.herokuapp.com/](https://student-union-project.herokuapp.com/)

## Pembagian Tugas
- Ihsan Muhammad Aulia: -
- Ramawajdi Kanishka Anwar : -
- Taliza Ayu Dwiputritaufik : -

## Group's Note:

### "How To Git-good" Starter Pack ((semoga jelas ya))

- Clone dulu reponya dengan cara: buka Command Prompt / Terminal, terus nasukkan `git clone https://gitlab.com/tp1-ppw-d-2018-kel-13/student-union.git`. Dengan ini, pada folder kalian nyalain Command Prompt / Terminal, akan terbentuk folder local repo untuk keperluan kalian push nanti

- Buat bikin branch baru dari master, kalian bisa pakai command `git checkout -b [nama branch]`. Dengan ini, kalian nggak bakal conflict gara-gara semuanya ngepush ke master nanti dibagi-bagi gitu.

- Coba dulu ngecommit terus push ke branch masing-masing dengan command  `git push origin [nama branch]` 

- Untuk menggabungkan hasil dari branch kalian ke master, ada panduan di web gitlab keywordnya `merge request`. Jangan lupa kabar-kabarin di grup line ya biar gaada conflict~!

- Kalau mau ubah-ubah codingannya lagi, pastiin udah balik ke branch masing-masing ya. Bisa ngecek branchnya ada apa aja dengan command `git branch`. Nanti bakal keluar daftar branch yang ada. 

- Kalau mau pindah ke branch (yang udah ada), bisa pakai command  `git checkout [nama branch]`.

- Pull ulang dari master `git pull origin master` biar up-to-date dengan berbagai update dari yang lain, kalau mau ngapa-ngapain lagi, kembali ke langkah 3 (commit and push to branch and merge request).

- Untuk menghapus local branch (branch di komputer masing-masing), bisa menggunakan command `git branch -d [nama branch]`.

- Cara delete remote branch (branch di gitlabnya) selain cara manual di web gitlabnya, bisa menggunakan command `git push [remote] -d [nama branch]`.

- **  Jangan lupa buat pull dari master rajin rajin sebelum bikin dari kalian~ **

### Python
- Nama class `CamelCase` atau `snake_case`
- Nama variable `[namaClass_namaVariable]` biar nanti nggak ngebingungin pas bikin di modelsnya. Example: `Models_News`
- Nama method nya `[nama class]_namaMethod` contoh `View_Register`















